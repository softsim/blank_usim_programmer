#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import struct,binascii
import sys


def unzipit(MCC, length):
	if length == 3:
		t = MCC + 111
	elif length == 2:
		t = MCC + 11
	else:
		raise Exception('Length Error...')

	r = []
	while t != 0:
		x = t % 10
		r.append('%d' % x)
		if x == 0:
			t = t - 10
		t = t//10
		
	return ''.join(r[::-1])

def decode_S1(S1):
	S1_1 = (S1 >> 14)  # & 0x03FF
	S1_2 = (S1 >> 10 & 0x0F)
	S1_3 = S1 & 0x03FF
	
	if S1_2 == 10:
		S1_2 = 0

	return unzipit(S1_1, 3) + ('%d' % S1_2) + unzipit(S1_3, 3)

def decode_IMSI_M(IMSI_M):
	S2, = struct.unpack('<H', IMSI_M[1:3])
	S1, = struct.unpack('<L', IMSI_M[3:6] + b'\x00')
	MNC, = struct.unpack('<B', IMSI_M[6:7])
	MCC, = struct.unpack('<H', IMSI_M[-2:])

	return unzipit(MCC, 3) + unzipit(MNC, 2) + 	unzipit(S2, 3) + decode_S1(S1)


#-----------------------------------------------------------------------
if len(sys.argv) != 2:
	print("i need one IMSI_M.")
	exit(-1)

IMSI_M = binascii.unhexlify(sys.argv[1])
imsi= decode_IMSI_M(IMSI_M)
print(imsi)
