#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import struct,binascii
import sys


def zipit(MCC):
	t = 0
	for c in MCC:
		x = int(c)
		if x == 0:
			x  = 10
		t = t*10 + x
	if len(MCC) == 3:
		t = t - 111
	else:
		t = t - 11
	return t

def encode_imsi_m(imsi):
	MCC = imsi[:3]
	MNC = imsi[3:5]
	S2 = imsi[5:8]
	S1_1 = imsi[8:11]
	S1_2 = imsi[11:12]
	S1_3 = imsi[12:15]	

	mcc = struct.pack('<H', zipit(MCC))
	mnc = struct.pack('<B', zipit(MNC))	
	s2 = struct.pack('<H', zipit(S2))
	#s1_long = zipit(S1_1) * 1024 * 16 + int(S1_2) * 1024 + zipit(S1_3)
	s1_2 = int(S1_2)
	if s1_2 == 0:
		s1_2 = 10
	s1_long = (zipit(S1_1) << 14) + (s1_2 << 10) + zipit(S1_3)
	s1 = struct.pack('<L', s1_long)
	imsi_m = b'\x00' + s2 + s1[:-1] + mnc + b'\x80' + mcc
	return imsi_m

#-----------------------------------------------------------------------
if len(sys.argv) != 2:
	print("i need one IMSI.")
	exit(-1)

imsi = sys.argv[1]
imsi_m = encode_imsi_m(imsi)
print(binascii.hexlify(imsi_m))
