#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import struct
from optparse import OptionParser

from smartcard.System import readers
from smartcard.Exceptions import NoCardException
from smartcard.util import toHexString, toBytes


def parse_options():
    parser = OptionParser(usage="usage: %prog [options]")

    parser.add_option("-l", "--list", dest="readerlist",  action='store_true',
            help="list all pc/sc readers",
            default=False,
        )

    parser.add_option("-d", "--detect", dest="detect",  action='store_true',
            help="detect card type and print",
            default=False,
        )

    parser.add_option("-r", "--reader", dest="readerindex",  metavar="IDX",
            type=int,  default=0,
            help="pc/sc reader index [default: %default]",
        )

    parser.add_option("-t", "--type", dest="type",
            help="Card type (user -t list to view) [default: %default]",
            default="auto",
        )

    parser.add_option("-n", "--name", dest="name",
            help="Operator name [default: %default]",
            default="auto",
        )

    parser.add_option("-m", "--smsc", dest="smsc",
            help="sms center number",
        )
        
    parser.add_option("-s", "--iccid", dest="iccid", metavar="ID",
            help="Integrated Circuit Card ID",
        )

    parser.add_option("-i", "--imsi", dest="imsi",
            help="International Mobile Subscriber Identity",
        )

    parser.add_option("-x", "--imsim", dest="imsim", metavar="MIN",
            help="CDMA IMSM_M",
        )

    parser.add_option("-u", "--uimid", dest="uimid", metavar="ID",
            help="CDMA UIM ID",
        )

    parser.add_option("-a", "--akey", dest="akey",
            help="CDMA AKey",
        )

    parser.add_option("-k", "--ki", dest="ki",
            help="Milenege Ki",
        )

    parser.add_option("-o", "--opc", dest="opc",
            help="Milenege OPC",
        )

    parser.add_option("-z", "--hrpdss", dest="hrpdss", metavar="SS",
            help="CDMA HRPD Share Secret",
        )

    parser.add_option("-j", "--adm", dest="adm", metavar="ADM1",
            help="ADM1 Password",
        )


    (options, args) = parser.parse_args()
    
    return options




SIM_SELECT = [0xA0, 0xA4, 0x00, 0x00, 0x02]
USIM_SELECT = [0x00, 0xA4, 0x00, 0x0C, 0x02]

VER_GSM_SIM_1  = [0x01, 0x02, 0x13, 0x01, 0x02, 0x56, 0x04, 0xFF, 0x13, 0x24, 0x11, 0x0A, 0x00, 0x86, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF]
VER_CDMA_UIM_0 = [0x03, 0xB3, 0x11, 0x01, 0x02, 0x45, 0x05, 0xFF, 0x00, 0xA2, 0x10, 0x0A, 0x86, 0x00, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF]
VER_CDMA_UIM_1 = [0x0C, 0x03, 0x12, 0x05, 0x02, 0x45, 0x05, 0xFF, 0x13, 0xA2, 0x11, 0x0A, 0x86, 0x00, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF]
VER_LTE_USIM_1 = [0x81, 0x06, 0x14, 0x07, 0x02, 0x10, 0x05, 0xFF, 0x14, 0x43, 0x00, 0x02, 0x00, 0x86, 0xDF, 0xDF, 0xDF, 0xDF, 0xDF, 0xDF]
VER_LTE_USIM_2 = [0x81, 0x06, 0x14, 0x07, 0x02, 0x10, 0x05, 0xFF, 0x14, 0x43, 0x00, 0x02, 0x00, 0x86, 0x22, 0x22, 0x22, 0x22, 0x22, 0x22]
VER_CSIM_A     = [0x20, 0x15, 0x06, 0x29, 0x22, 0x8F, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF]
VER_CSIM_B     = [0x20, 0x16, 0x08, 0x11, 0x22, 0x8F, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF]

MF = [0x3F, 0x00]
EF_VERSION_A = [0xA0, 0x00]
EF_VERSION_B = [0xB0, 0x00]
EF_ICCID = [0x2F, 0xE2]

DF_TELECOM = [0x7F, 0x10]
EF_SMSP = [0x6F, 0x42]
EF_CMCC_BRAND = [0x6F, 0xFE]

DF_GSM = [0x7F, 0x20]
EF_GSM_KI     = [0x00, 0x02]
EF_IMSI       = [0x6F, 0x07]
EF_PLMN_SEL   = [0x6F, 0x30]
EF_SETUP_MENU = [0x6F, 0x54]
EF_ACC        = [0x6F, 0x78]
EF_FPLMN      = [0x6F, 0x7B]
EF_SPN        = [0x6F, 0x46]

DF_CDMA = [0x7F, 0x25]
EF_AKEY     = [0x00, 0x05]
EF_SSD      = [0x00, 0x06]
EF_HRPD_SS  = [0x00, 0x0A]
EF_IMSI_M   = [0x6F, 0x22]
EF_HOME_SID = [0x6f, 0x28]
EF_ACCLOC   = [0x6F, 0x2C]
EF_UIMID    = [0x6F, 0x31]
EF_HRPD_UPP = [0x6F, 0x57]

DF_USIM_F = [0x7F, 0xFF]
EF_SJS_OPC = [0, 0xF7]
EF_SJS_KI  = [0, 0xFF]
DF_USIM_SJS = [0x7F, 0xCC]
EF_R1R5_SJS = [0x6F, 0x01]

DF_USIM = [0x7F, 0xF0]
EF_LOCI   = [0x6F, 0x7E]
EF_EHPLMN = [0x6F, 0xD9]
EF_LTE_KI     = [0xFF, 0x02]
EF_LTE_OPC    = [0xFF, 0x01]
EF_LTE_R1R5   = [0xFF, 0x03]
EF_SQN        = [0xFF, 0x06]
EF_MULTI_IMSI = [0x9F, 0x01]

DF_CSIM = [0x7F, 0xF1]
EF_CDMA_SPN = [0x6F, 0x41]


SIM_ATR = []


cmcc = ('46000', '46002', '46004', '46007', '46008')


def ascii_to_list(string):
	rc = []
	for c in string:
		rc.append(ord(c))
	return rc

def encode_iccid(digit_str=''):
    digit_str = digit_str[:20] + 'F'*(20-len(digit_str[:20]))
    return [(int(digit_str[i+1],16)<<4)+int(digit_str[i],16) \
            for i in range(0, 20, 2)]


def encode_imsi(imsi):
    l = (len(imsi) + 1) // 2    # Required bytes
    oe = len(imsi) & 1          # Odd (1) / Even (0)
    digit_str = ('%01x' % ((oe<<3)|1)) + imsi
    return [l] + [(int(digit_str[i+1],16)<<4)+int(digit_str[i],16) \
            for i in range(0, 16, 2)]

def encode_smsp(smsc, operator_len = 12):
    _al = operator_len * [0xFF]    #alpha Operator Name
    _p_ind = [0xFD]      # Parameters indicator : only SMSC phone number
    _dest_addr = 12*[0xFF] # blank TP-Destination address

    l = (len(smsc) + 3) // 2    # Required bytes
    prefix_flag = 0x91
    digit_str = smsc[:20] + 'F'*(20-len(smsc[:20]))
    _sc_addr = [l, prefix_flag] +  [(int(digit_str[i+1],16)<<4)+int(digit_str[i],16) \
            for i in range(0, 20, 2)]
            
    _pid = [0xFF] # blank protocol ID
    _dcs = [0xFF] # Data Coding Scheme 
    _val = [0xFF] # blank validity period
    
    SMSP = _al + _p_ind + _dest_addr + _sc_addr + _pid + _dcs + _val   
    
    return SMSP 
    


def print_readers():
    sim_readers = readers() 
    if len(sim_readers) == 0:
        print('No smart card reader.')
    else:
        for i, r in enumerate(sim_readers, 0):
            print('%d:  %s' % (i, r))

def connnect_card(index):
    r = readers()
    if len(r) == 0:
        print('No PC/SC Reader.')
        return None

    connection = r[index].createConnection()
    connection.connect()
    global SIM_ATR
    SIM_ATR = connection.getATR()
    print('ATR: ', toHexString(SIM_ATR))
    
    return connection



    

def detect_card_type(connection):
    if SIM_ATR == [0x3B, 0x9F, 0x96, 0x80, 0x1F, 0xC7, 0x80, 0x31, 0xA0, 0x73, 0xBE, 0x21, 0x13, 0x67, 0x43, 0x20, 0x07, 0x18, 0x00, 0x00, 0x01, 0xA5]:
        return 6

    _, sw1, sw2 = connection.transmit(SIM_SELECT + MF)
    #print("%02X %02X" % (sw1, sw2))
    if sw1 != 0x9F:
        return -1

    _, sw1, sw2 = connection.transmit(SIM_SELECT + EF_VERSION_A)
    #print("%02X %02X" % (sw1, sw2))
    if sw1 == 0x9F:
        data, sw1, sw2 = connection.transmit([0xA0, 0xB0, 0x00, 0x00, 0x14])
        #print("%02X %02X" % (sw1, sw2))

        if (data > VER_GSM_SIM_1) - (data < VER_GSM_SIM_1)  == 0:
            return 1
        elif (data > VER_CDMA_UIM_1) - (data < VER_CDMA_UIM_1) == 0:
            return 2
        elif (data > VER_CDMA_UIM_0) - (data < VER_CDMA_UIM_0) == 0:
            return 3
        elif (data > VER_LTE_USIM_1) - (data < VER_LTE_USIM_1) == 0:
            return 4
        elif (data > VER_LTE_USIM_2) - (data < VER_LTE_USIM_2) == 0:
            return 7
        elif (data > VER_CSIM_A) - (data < VER_CSIM_A) == 0:
            return 5
        else:
            return -1
       
    else:
        _, sw1, sw2 = connection.transmit(SIM_SELECT + EF_VERSION_B)
        #print("%02X %02X" % (sw1, sw2))        
        if sw1 != 0x9F:
            if SIM_ATR == [0x3B, 0x9C, 0x94, 0x00, 0x68, 0x86, 0x8D, 0x0A, 0x86, 0x98, 0x02, 0x56, 0xC2, 0x00, 0x05, 0x00]:
                return 1
            elif  SIM_ATR == [0x3B, 0x9F, 0x94, 0x80, 0x1F, 0xC7, 0x80, 0x31, 0xE0, 0x73, 0xFE, 0x21, 0x13, 0x57, 0x86, 0x81, 0x06, 0x86, 0x98, 0x40, 0x18, 0xAD]:
                return 4
            else:
                print("fuck")
                return -1
        
        data, sw1, sw2 = connection.transmit([0xA0, 0xB0, 0x00, 0x00, 0x14])
        if (data > VER_CSIM_B) - (data < VER_CSIM_B):
            return -1
        return 5





def verify_chv(conn, data, adm=0xA, cls=0xA0):
    VERIFY = [cls, 0x20, 0x00, adm, len(data)] + data
    #print(toHexString(VERIFY))
    _, sw1, sw2 = conn.transmit(VERIFY)
    #print("%02X %02X" % (sw1, sw2))
    if sw1 == 0x90:
        return True
    else:
        return False

def update_binary(conn, data, cls=0xA0, P1=0x00, P2=0x00):
    UPDATE_BINARY = [cls, 0xD6, P1, P2, len(data)] + data
    #print(toHexString(UPDATE_BINARY))
    _, sw1, sw2 = conn.transmit(UPDATE_BINARY)
    print("update binary: %02X %02X" % (sw1, sw2))
    if sw1 == 0x90:
        return True
    else:
        return False

def update_record(conn, record, cls=0xA0, P1=0x01, P2=0x04):
    UPDATE_RECORD = [cls, 0xDC, P1, P2, len(record)] + record
    #print(toHexString(UPDATE_RECORD))
    _, sw1, sw2 = conn.transmit(UPDATE_RECORD)
    print("update record: %02X %02X" % (sw1, sw2))
    if sw1 == 0x90:
        return True
    else:
        return False

def get_plmn(imsi):
    cucc = ('46001', '46006', '46009', '46010')
    ctcc = ('46003', '46011', '46012')
    oai  = ('20892', '20893')

    plmn_sel = [0x64, 0xF0, 0x10, 0x64, 0xF0, 0x60, 0x64, 0xF0, 0x90, 0x64, 0xF0, 0x01] + 4 * [0xFF]
    spn = [0x02, 0x80, 0x4E, 0x2D, 0x56, 0xFD, 0x80, 0x54, 0x90, 0x1A]

    if imsi[:5] in cucc:
        print('----cucc----')
    elif imsi[:5] in cmcc:
        print('----cmcc----')
        plmn_sel = [0x64, 0xF0, 0x00, 0x64, 0xF0, 0x20, 0x64, 0xF0, 0x70, 0x64, 0xF0, 0x80] + 4 * [0xFF]
        spn = [0x02, 0x80, 0x4E, 0x2D, 0x56, 0xFD, 0x79, 0xFB, 0x52, 0xA8]
    elif imsi[:5] in ctcc:
        print('----ctc----')
        plmn_sel = [0x64, 0xF0, 0x11, 0x64, 0xF0, 0x30, 0x64, 0xF0, 0x21] + 7 * [0xFF]
        spn = [0x02, 0x80, 0x4E, 0x2D, 0x56, 0xFD, 0x75, 0x35, 0x4F, 0xE1]
    elif imsi[:5] in oai:
        plmn_sel = [0x02, 0xF8, 0x29, 0x02, 0xF8, 0x39] + 10 * [0xFF]
        spn = [0x00, 0x54, 0x45, 0x53, 0x54]
    else:
        print('plmn may be not supportd, defaults set to cucc')
    return (plmn_sel, spn)

def program_gsm_sim(conn, iccid, imsi, ki, smsc):
    # schh0812 
    ADM6 = [0x73, 0x63, 0x68, 0x68, 0x30, 0x38, 0x31, 0x32]
    if not verify_chv(conn, ADM6, 0x06):
        print('ADM 6 Error')
        return -1
    # 12345678
    ADM5 = [0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38]
    if not verify_chv(conn, ADM5, 0x05):
        print('ADM 5 Error')
        return -2

    _, sw1, sw2 = conn.transmit(SIM_SELECT + MF)
    _, sw1, sw2 = conn.transmit(SIM_SELECT + EF_ICCID)
    _iccid = encode_iccid(iccid)
    update_binary(conn, _iccid)

    _, sw1, sw2 = conn.transmit(SIM_SELECT + DF_TELECOM)
    _, sw1, sw2 = conn.transmit(SIM_SELECT + EF_SMSP)
    _smsp = encode_smsp(smsc)
    update_record(conn, _smsp)

    _, sw1, sw2 = conn.transmit(SIM_SELECT + EF_CMCC_BRAND)
    #print("switch brand : %02X %02X" % (sw1, sw2))
    brand = 0x42 * [0xFF]
    update_binary(conn, brand)

    _, sw1, sw2 = conn.transmit(SIM_SELECT + DF_GSM)
    #print("switch gsm : %02X %02X" % (sw1, sw2))
    _, sw1, sw2 = conn.transmit(SIM_SELECT + EF_IMSI)
    _imsi = encode_imsi(imsi)
    update_binary(conn, _imsi)

    _, sw1, sw2 = conn.transmit(SIM_SELECT + EF_ACC)
    acc = [0x00, 0x20]
    update_binary(conn, acc)

    _, sw1, sw2 = conn.transmit(SIM_SELECT + EF_SETUP_MENU)
    #print("switch menu : %02X %02X" % (sw1, sw2))
    menu = 0x17 * [0xFF]
    update_binary(conn, menu)

    _, sw1, sw2 = conn.transmit(SIM_SELECT + EF_FPLMN)
    fplmn = 0xC * [0xFF]
    update_binary(conn, fplmn)

    plmn_sel, spn = get_plmn(imsi)
    _, sw1, sw2 = conn.transmit(SIM_SELECT + EF_PLMN_SEL)
    update_binary(conn, plmn_sel)

    _, sw1, sw2 = conn.transmit(SIM_SELECT + EF_SPN)
    update_binary(conn, spn)

    _, sw1, sw2 = conn.transmit(SIM_SELECT + EF_GSM_KI)
    _ki = toBytes(ki)
    update_binary(conn, _ki)

    #---the end-----






def zipit(MCC):
    t = 0
    for c in MCC:
        x = int(c)
        if x == 0:
            x  = 10
        t = t*10 + x

    if len(MCC) == 3:
        t = t - 111
    else:
        t = t - 11
    return t

def encode_imsi_m(imsi):
    MCC = imsi[:3]
    MNC = imsi[3:5]
    S2 = imsi[5:8]
    S1_1 = imsi[8:11]
    S1_2 = imsi[11:12]
    S1_3 = imsi[12:15]

    mcc = struct.pack('<H', zipit(MCC))
    mnc = struct.pack('<B', zipit(MNC))
    s2 = struct.pack('<H', zipit(S2))

    s1_2 = int(S1_2)
    if s1_2 == 0:
        s1_2 = 10
    s1_long = (zipit(S1_1) << 14) + (s1_2 << 10) + zipit(S1_3)
    s1 = struct.pack('<L', s1_long)

    imsi_m = b'\x00' + s2 + s1[:-1] + mnc + b'\x80' + mcc
    return [x for x in imsi_m]

def encode_uimid(uimid):
    esn = toBytes(uimid)[::-1]
    l = len(esn)
    p = 8 - l -1
    return [l] + esn + p * [0]


def encode_hrpd_upp(imsi):
    upp = bytes(imsi + '@mycdma.cn',  encoding="utf8")
    l = len(upp)
    p = 0x31 - l - 3
    return [l+2, l] + [x for x in upp] + [0] + p * [0xFF]

def encode_hrpd_ss(ss):
    hrpdss = bytes(ss,  encoding="utf8")
    l = len(hrpdss)
    return [l] + [x for x in hrpdss]

def program_cdma_uim(conn, iccid, imsi, akey, uimid, ss):
    # schh0812
    ADM6 = [0x73, 0x63, 0x68, 0x68, 0x30, 0x38, 0x31, 0x32]
    if not verify_chv(conn, ADM6, 0x06):
        print('ADM 6 Error')
        return -1
    # 74195393
    ADM5 = [0x37, 0x34, 0x31, 0x39, 0x35, 0x33, 0x39, 0x33]
    if not verify_chv(conn, ADM5, 0x05):
        print('ADM 5 Error')
        return -2

    _, sw1, sw2 = conn.transmit(SIM_SELECT + MF)
    _, sw1, sw2 = conn.transmit(SIM_SELECT + EF_ICCID)
    _iccid = encode_iccid(iccid)
    update_binary(conn, _iccid)

    _, sw1, sw2 = conn.transmit(SIM_SELECT + DF_CDMA)
    _, sw1, sw2 = conn.transmit(SIM_SELECT + EF_IMSI_M)
    _imsim = encode_imsi_m(imsi)
    update_binary(conn, _imsim)

    _, sw1, sw2 = conn.transmit(SIM_SELECT + EF_ACCLOC)
    acc = [0x09]
    update_binary(conn, acc)

    _, sw1, sw2 = conn.transmit(SIM_SELECT + EF_UIMID)
    _uimid = encode_uimid(uimid)
    update_binary(conn, _uimid)

    _, sw1, sw2 = conn.transmit(SIM_SELECT + EF_AKEY)
    _akey = toBytes(akey)
    update_binary(conn, _akey)

    _, sw1, sw2 = conn.transmit(SIM_SELECT + EF_HRPD_UPP)
    _upp = encode_hrpd_upp(imsi)
    update_binary(conn, _upp)

    _, sw1, sw2 = conn.transmit(SIM_SELECT + EF_HRPD_SS)
    _ss = encode_hrpd_ss(ss)
    update_record(conn, _ss)

    _, sw1, sw2 = conn.transmit(SIM_SELECT + EF_SSD)
    ssd = 16 * [0]
    update_record(conn, ssd)
    update_record(conn, ssd,  P1=0x02)


def program_usim_sjs(conn, adm, iccid, imsi, ki, opc, smsc):
    #verify ADM-1
    ADM_A = ascii_to_list(adm)
    if not verify_chv(conn, ADM_A, adm=0x0A, cls=0x00):
        print('ADM A Error')
        return -2

    _, sw1, sw2 = conn.transmit(USIM_SELECT + MF)
    _, sw1, sw2 = conn.transmit(USIM_SELECT + EF_ICCID)
    _iccid = encode_iccid(iccid)
    update_binary(conn, _iccid,  cls=0x00)

    _, sw1, sw2 = conn.transmit(USIM_SELECT + DF_GSM)
    _, sw1, sw2 = conn.transmit(USIM_SELECT + EF_IMSI)
    _imsi = encode_imsi(imsi)
    update_binary(conn, _imsi,  cls=0x00)

    _, sw1, sw2 = conn.transmit(USIM_SELECT + EF_SJS_OPC)
    _opc = toBytes(opc)
    update_binary(conn, _opc, cls=0x00, P1=0, P2=1) #offset 01

    _, sw1, sw2 = conn.transmit(USIM_SELECT + EF_SJS_KI)
    _ki = toBytes(ki)
    update_binary(conn, _ki, cls=0x00)

    plmn_sel, spn = get_plmn(imsi)
    _, sw1, sw2 = conn.transmit(USIM_SELECT + EF_SPN)
    update_binary(conn, spn, cls=0x00)
    _, sw1, sw2 = conn.transmit(USIM_SELECT + EF_PLMN_SEL)
    update_binary(conn, plmn_sel, cls=0x00)

    _, sw1, sw2 = conn.transmit(USIM_SELECT + EF_LOCI)
    loci = [0xFF, 0xFF, 0xFF, 0xFF, 0x64, 0xF0, 0x00, 0x00, 0x00, 0xFF, 0x01]
    update_binary(conn, loci, cls=0x00)

    _, sw1, sw2 = conn.transmit(USIM_SELECT + EF_FPLMN)
    fplmn = 0xC * [0xFF]
    update_binary(conn, fplmn, cls=0x00)

    _, sw1, sw2 = conn.transmit(USIM_SELECT + DF_USIM_SJS)
    _, sw1, sw2 = conn.transmit(USIM_SELECT + EF_R1R5_SJS)
    if imsi[:5] in cmcc:
        r1r5 = [0x20, 0x13, 0x2F, 0x49, 0x5B]
    else:
        print('---R1R5 normal--')
        r1r5 = [0x40, 0x00, 0x20, 0x40, 0x60]
    update_binary(conn, r1r5, cls=0x00, P1=0, P2=0x50)

    _, sw1, sw2 = conn.transmit(USIM_SELECT + DF_TELECOM)
    _, sw1, sw2 = conn.transmit(USIM_SELECT + EF_SMSP)
    _smsp = encode_smsp(smsc, 44)
    update_record(conn, _smsp, cls=0x00)





def program_lte_usim(conn, iccid, imsi, ki, opc, smsc):
    #switch to root mode
    cmd72 = [0xA0, 0x72, 0x00, 0x00, 0x10, 0xE1, 0x6C, 0xD6, 0xAE, 0x05, 0x8B, 0x03, 0x2F, 0x06, 0x01, 0xC6, 0x09, 0x90, 0x01, 0x40, 0x83]
    cmd74 = [0xA0, 0x74, 0x00, 0x02, 0x00]
    _, sw1, sw2 = conn.transmit(cmd72)
    _, sw1, sw2 = conn.transmit(cmd74)
    conn.disconnect()
    conn.connect()

    # 543_987A
    ADM_B = [0x35, 0x34, 0x33, 0x14, 0x39, 0x38, 0x37, 0x41]
    if not verify_chv(conn, ADM_B, adm=0x0B, cls=0x00):
        print('ADM B Error')
        return -1
    # A78_4345
    ADM_A = [0x41, 0x37, 0x38, 0x19, 0x34, 0x33, 0x34, 0x35]
    if not verify_chv(conn, ADM_A, adm=0x0A, cls=0x00):
        print('ADM A Error')
        return -2

    _, sw1, sw2 = conn.transmit(USIM_SELECT + MF)
    _, sw1, sw2 = conn.transmit(USIM_SELECT + EF_ICCID)
    _iccid = encode_iccid(iccid)
    update_binary(conn, _iccid,  cls=0x00)

    _, sw1, sw2 = conn.transmit(USIM_SELECT + DF_USIM)
    _, sw1, sw2 = conn.transmit(USIM_SELECT + EF_IMSI)
    _imsi = encode_imsi(imsi)
    update_binary(conn, _imsi,  cls=0x00)

    _, sw1, sw2 = conn.transmit(USIM_SELECT + EF_SMSP)
    _smsp = encode_smsp(smsc)
    update_record(conn, _smsp, cls=0x00)

    _, sw1, sw2 = conn.transmit(USIM_SELECT + EF_ACC)
    acc = [0x01, 0x00]  #cucc 0x02 0x00
    update_binary(conn, acc, cls=0x00)

    plmn_sel, spn = get_plmn(imsi)
    _, sw1, sw2 = conn.transmit(USIM_SELECT + EF_SPN)
    update_binary(conn, spn, cls=0x00)

    _, sw1, sw2 = conn.transmit(USIM_SELECT + EF_LOCI)
    loci = [0xFF, 0xFF, 0xFF, 0xFF, 0x64, 0xF0, 0x00, 0x00, 0x00, 0xFF, 0x01]
    update_binary(conn, loci, cls=0x00)

    _, sw1, sw2 = conn.transmit(USIM_SELECT + EF_EHPLMN)
    ehplmn = plmn_sel[:3] + 9 * [0xFF]
    update_binary(conn, ehplmn, cls=0x00)

    _, sw1, sw2 = conn.transmit(USIM_SELECT + EF_LTE_OPC)
    _opc = toBytes(opc)
    update_binary(conn, _opc, cls=0x00)

    _, sw1, sw2 = conn.transmit(USIM_SELECT + EF_LTE_KI)
    _ki = toBytes(ki)
    update_binary(conn, _ki, cls=0x00)

    _, sw1, sw2 = conn.transmit(USIM_SELECT + EF_LTE_R1R5)
    #print(plmn_sel)
    if imsi[:5] in cmcc:
        r1r5 = [0x20, 0x13, 0x2F, 0x49, 0x5B]
    else:
        print('---R1R5 normal--')
        r1r5 = [0x40, 0x00, 0x20, 0x40, 0x60]
    update_binary(conn, r1r5, cls=0x00)

    _, sw1, sw2 = conn.transmit(USIM_SELECT + EF_SQN)
    sqn = 6 * [0]
    for i in range(1,34):
        update_record(conn, sqn, cls=0x00, P1=i)

    ### FPLMN #####
    _, sw1, sw2 = conn.transmit(USIM_SELECT + EF_FPLMN)
    fplmn = 0xC * [0xFF]
    update_binary(conn, fplmn, cls=0x00)

    _, sw1, sw2 = conn.transmit(USIM_SELECT + DF_GSM)
    _, sw1, sw2 = conn.transmit(USIM_SELECT + EF_PLMN_SEL)
    update_binary(conn, plmn_sel, cls=0x00)

    #verify_chv(conn, ADM_B, adm=0x0B, cls=0x00)
    #conn.disconnect()
    #conn.connect()
    #cmd_exit =[0xA0, 0x74, 0x00, 0x01, 0x00]
    #_, sw1, sw2 = conn.transmit(cmd72)
    #--------------------------the end-----------------------------------






def program_csim(conn, iccid, imsi, ki, opc, imsim, akey, uimid, hrpdss):
    #switch to root mode
    cmd72 = [0xA0, 0x72, 0x00, 0x00, 0x10, 0x42, 0x48, 0x44, 0x43, 0x4E, 0x41, 0x4E, 0x46, 0x41, 0x4E, 0x47, 0x58, 0x49, 0x4C, 0x49, 0x45]
    cmd74 = [0xA0, 0x74, 0x00, 0x02, 0x00]
    _, sw1, sw2 = conn.transmit(cmd72)
    _, sw1, sw2 = conn.transmit(cmd74)
    conn.disconnect()
    conn.connect()

    # y&qT>
    ADM_B = [0x79, 0x26, 0x71, 0x54, 0x3E, 0xB3, 0x62, 0xAA]      # 792671543EB362AA
    if not verify_chv(conn, ADM_B, adm=0x0B, cls=0x00):
        print('ADM B Error')
        return -1
    # 22222222
    #ADM_A = [0x32, 0x32, 0x31, 0x32, 0x32, 0x32, 0x32, 0x32]      # 3232313232323232
    ADM_A = [0x32, 0x32, 0x32, 0x32, 0x32, 0x32, 0x32, 0x32]
    if not verify_chv(conn, ADM_A, adm=0x0A, cls=0x00):
        print('ADM A Error')
        return -2

    _, sw1, sw2 = conn.transmit(USIM_SELECT + MF)
    _, sw1, sw2 = conn.transmit(USIM_SELECT + EF_ICCID)
    _iccid = encode_iccid(iccid)
    update_binary(conn, _iccid,  cls=0x00)

#===========================================================
    _, sw1, sw2 = conn.transmit(USIM_SELECT + DF_CDMA)
    _, sw1, sw2 = conn.transmit(USIM_SELECT + EF_IMSI_M)
    _imsim = encode_imsi_m(imsim)
    #print(toHexString(_imsim))
    update_binary(conn, _imsim,  cls=0x00)

    _, sw1, sw2 = conn.transmit(USIM_SELECT + EF_ACCLOC)
    acc = [0x00]
    update_binary(conn, acc,  cls=0x00)

    _, sw1, sw2 = conn.transmit(USIM_SELECT + EF_UIMID)
    _uimid = encode_uimid(uimid)
    #print(toHexString(_uimid))
    update_binary(conn, _uimid,  cls=0x00)

    _, sw1, sw2 = conn.transmit(USIM_SELECT + EF_AKEY)
    _akey = toBytes(akey)
    update_binary(conn, _akey,  cls=0x00)

    _, sw1, sw2 = conn.transmit(USIM_SELECT + EF_HRPD_UPP)
    _upp = encode_hrpd_upp(imsi)
    update_binary(conn, _upp,  cls=0x00)

    _, sw1, sw2 = conn.transmit(USIM_SELECT + EF_HRPD_SS)
    _ss = encode_hrpd_ss(hrpdss)
    update_record(conn, _ss,  cls=0x00)


    _, sw1, sw2 = conn.transmit(USIM_SELECT + EF_SSD)
    ssd_data = 10 * [0xFF]
    for i in range(1, 5):
        update_record(conn, ssd_data, cls=0x00, P1=i)

#===========================================================
    _, sw1, sw2 = conn.transmit(USIM_SELECT + DF_USIM)
    _, sw1, sw2 = conn.transmit(USIM_SELECT + EF_IMSI)
    _imsi = encode_imsi(imsi)
    update_binary(conn, _imsi,  cls=0x00)

    #_, sw1, sw2 = conn.transmit(USIM_SELECT + EF_SMSP)
    #_smsp = encode_smsp(smsc)
    #update_record(conn, _smsp, cls=0x00)

    _, sw1, sw2 = conn.transmit(USIM_SELECT + EF_ACC)
    acc = [0x00, 0x01]
    update_binary(conn, acc, cls=0x00)

    plmn_sel, spn = get_plmn(imsi)
    _, sw1, sw2 = conn.transmit(USIM_SELECT + EF_SPN)
    update_binary(conn, spn, cls=0x00)

    _, sw1, sw2 = conn.transmit(USIM_SELECT + EF_LOCI)
    loci = [0xFF, 0xFF, 0xFF, 0xFF, 0x64, 0xF0, 0x00, 0x00, 0x00, 0xFF, 0x01]
    update_binary(conn, loci, cls=0x00)

    _, sw1, sw2 = conn.transmit(USIM_SELECT + EF_EHPLMN)
    ehplmn = plmn_sel[:6] + 6 * [0xFF]
    update_binary(conn, ehplmn, cls=0x00)

    _, sw1, sw2 = conn.transmit(USIM_SELECT + EF_LTE_OPC)
    _opc = toBytes(opc)
    update_binary(conn, _opc, cls=0x00)

    _, sw1, sw2 = conn.transmit(USIM_SELECT + EF_LTE_KI)
    _ki = toBytes(ki)
    update_binary(conn, _ki, cls=0x00)

    _, sw1, sw2 = conn.transmit(USIM_SELECT + EF_LTE_R1R5)
    if imsi[:5] in cmcc:
        r1r5 = [0x20, 0x13, 0x2F, 0x49, 0x5B]
    else:
        print('---r1r5 normal---')
        r1r5 = [0x40, 0x00, 0x20, 0x40, 0x60]
    update_binary(conn, r1r5, cls=0x00)

    _, sw1, sw2 = conn.transmit(USIM_SELECT + EF_SQN)
    sqn = 6 * [0]
    for i in range(1, 34):
        update_record(conn, sqn, cls=0x00, P1=i)

    ### FPLMN #####
    _, sw1, sw2 = conn.transmit(USIM_SELECT + EF_FPLMN)
    fplmn = 0xC * [0xFF]
    update_binary(conn, fplmn, cls=0x00)

    ## EF_MULTSIM
    _, sw1, sw2 = conn.transmit(USIM_SELECT + EF_MULTI_IMSI)
    multisim1 =  _imsi + 60 * [0xFF]
    update_record(conn, multisim1, cls=0x00)

    ## 270A  6F00
    SELECT_XYZ = [0x00, 0xA4, 0x08, 0x0C, 0x04, 0x27, 0x0A, 0x6F, 0x00]
    _, sw1, sw2 = conn.transmit(SELECT_XYZ)
    xyz = [0x01]
    update_binary(conn, xyz, cls=0x00)

    #_, sw1, sw2 = conn.transmit(USIM_SELECT + DF_GSM)
    #_, sw1, sw2 = conn.transmit(USIM_SELECT + EF_PLMN_SEL)
    #update_binary(conn, plmn_sel, cls=0x00)

    _, sw1, sw2 = conn.transmit(USIM_SELECT + DF_CSIM)
    _, sw1, sw2 = conn.transmit(USIM_SELECT + EF_CDMA_SPN)
    cmda_spn = [0x01, 0x04, 0x06] + spn[2:]
    update_binary(conn, cmda_spn, cls=0x00)




CardType = {
    1: 'GSM SIM',
    2: 'CDMA UIM',
    3: 'CDMA UIM (2011)',
    4: 'WCDMA/LTE USIM',
    5: 'CDMA+LTE CSIM',
    6: 'sysmoUSIM-SJS1',
    7: 'LTE USIM-2',
    }


if __name__ == '__main__':
    opts = parse_options()

    if opts.readerlist:
        print_readers()
        exit(0)

    try:
        conn = connnect_card(opts.readerindex)
    except NoCardException:
        print('card index:',  opts.readerindex, '  >>>no card inserted')
        exit(-1)

    if conn == None:
        exit(-2)

    x = detect_card_type(conn)
    try:
        print('Card Type:', CardType[x])
    except:
        print('Unkown Card')
    if not opts.detect:
        if 1 == x:
            program_gsm_sim(conn, opts.iccid, opts.imsi, opts.ki, opts.smsc)
            print("GSM Progamming OK.")
        elif (2 == x) or (3 == x):
            program_cdma_uim(conn, opts.iccid, opts.imsim, opts.akey, opts.uimid, opts.hrpdss)
            print("CDMA Progamming OK.")
        elif 4 == x:
            program_lte_usim(conn, opts.iccid, opts.imsi, opts.ki, opts.opc, opts.smsc)
            print("LTE/WCDMA usim Progamming OK.")
        elif 5 == x:
            program_csim(conn, opts.iccid, opts.imsi, opts.ki, opts.opc, opts.imsim, opts.akey, opts.uimid, opts.hrpdss)
            print("CSIM Progamming OK.")

        elif 6 == x:
            program_usim_sjs(conn, opts.adm, opts.iccid, opts.imsi, opts.ki, opts.opc, opts.smsc)


